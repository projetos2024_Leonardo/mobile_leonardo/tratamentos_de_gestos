import React, { useState } from 'react';
import { View, FlatList, Image, Dimensions } from 'react-native';

const images = [
  { id: 1, uri: 'https://via.placeholder.com/200' },
  { id: 2, uri: 'https://via.placeholder.com/200/FF5733' },
  { id: 3, uri: 'https://via.placeholder.com/200/33FF57' },
  { id: 4, uri: 'https://via.placeholder.com/200/5733FF' },
];

const ImageSlider = () => {
  const [currentImageIndex, setCurrentImageIndex] = useState(0);

  const renderItem = ({ item }) => (
    <Image
      style={{ width: Dimensions.get('window').width, height: 200 }}
      source={{ uri: item.uri }}
    />
  );

  const handleScroll = event => {
    const { contentOffset, layoutMeasurement, contentSize } = event.nativeEvent;
    const currentIndex = Math.round(contentOffset.x / layoutMeasurement.width);
    setCurrentImageIndex(currentIndex);
  };

  return (
    <View>
      <FlatList
        horizontal
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        data={images}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        onScroll={handleScroll}
        scrollEventThrottle={200}
      />
      <View style={{ alignItems: 'center' }}>
        {images.map((_, index) => (
          <View
            key={index}
            style={{
              width: 8,
              height: 8,
              borderRadius: 4,
              backgroundColor: index === currentImageIndex ? '#007bff' : '#ccc',
              margin: 5,
            }}
          />
        ))}
      </View>
    </View>
  );
};

export default ImageSlider;
